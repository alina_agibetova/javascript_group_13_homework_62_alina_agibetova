import { Component, OnInit } from '@angular/core';
import { Game } from '../../shared/game.modal';
import { GameService } from '../../shared/game.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-detail-item',
  templateUrl: './detail-item.component.html',
  styleUrls: ['./detail-item.component.css']
})
export class DetailItemComponent implements OnInit {
  game!: Game;
  games!: Game[];

  constructor(private route: ActivatedRoute, private gameService: GameService) {
  }

  ngOnInit(): void {
    this.games = this.gameService.getGames();
    this.gameService.gamesChange.subscribe((games: Game[]) => {
      this.games = games;
    });
    this.route.params.subscribe((params: Params) => {
      const Id = params['name'];
      this.games.forEach((game: Game)=> {
        if (Id === game.name){
          this.game = game;
        }
      });
    });

  }
}
