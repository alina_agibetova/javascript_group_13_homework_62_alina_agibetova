import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ListComponent } from './list/list.component';
import { ItemComponent } from './list/item/item.component';
import { NewGameComponent } from './new-game/new-game.component';
import { DetailsComponent } from './details/details.component';
import { DetailItemComponent } from './details/detail-item/detail-item.component';
import { GameService } from './shared/game.service';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    ListComponent,
    ItemComponent,
    NewGameComponent,
    DetailsComponent,
    DetailItemComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [GameService],
  bootstrap: [AppComponent]
})
export class AppModule { }
