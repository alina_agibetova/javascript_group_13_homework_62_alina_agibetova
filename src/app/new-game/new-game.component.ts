import { Component, ElementRef, ViewChild } from '@angular/core';
import { GameService } from '../shared/game.service';
import { Game } from '../shared/game.modal';
import { PLATFORMS } from '../shared/platform';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.css']
})
export class NewGameComponent{
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('imageInput') imageInput!: ElementRef;
  @ViewChild('platformSelect') platformSelect!: ElementRef;
  @ViewChild('descriptionInput') descriptionInput!: ElementRef;
  selectOfPlatforms = PLATFORMS;

  constructor(public gameService: GameService){}


  createGame(){
    const name = this.nameInput.nativeElement.value;
    const image = this.imageInput.nativeElement.value;
    const select = this.platformSelect.nativeElement.value;
    const description = this.descriptionInput.nativeElement.value;


    const game = new Game(name,image, select, description);
    this.gameService.addGame(game);
  }
}
