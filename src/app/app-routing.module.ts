import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './list/list.component';
import { NewGameComponent } from './new-game/new-game.component';
import { ItemComponent } from './list/item/item.component';
import { DetailsComponent } from './details/details.component';
import { DetailItemComponent } from './details/detail-item/detail-item.component';

const routes: Routes = [
  {path: 'list', component: ListComponent, children: [
      {path: 'new', component: NewGameComponent},
      {path: ':platform', component: ItemComponent},
      {path: ':platform/:name', component: DetailItemComponent},
    ]},
  {path: 'details', component: DetailsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
