import { Component, OnInit } from '@angular/core';
import { Game } from '../shared/game.modal';
import { GameService } from '../shared/game.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  games!: Game[];
  gamePlatform: string[] = [];

  constructor(private gameService: GameService) {}

  ngOnInit(): void {
    this.games = this.gameService.getGames();
    this.gameService.gamesChange.subscribe((games: Game[])=> {
      this.games = games;
      this.getPlatform();
    });
    this.getPlatform();
  }

  getPlatform(){
    this.games.forEach((game: Game)=>{
      let platform = this.gamePlatform.some(elem => elem === game.platform);
      if (!platform){
        this.gamePlatform.push(game.platform);
      }
    })
  }

}
