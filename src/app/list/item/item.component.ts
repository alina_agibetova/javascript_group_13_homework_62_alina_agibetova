import { Component, OnInit } from '@angular/core';
import { Game } from '../../shared/game.modal';
import { ActivatedRoute, Params } from '@angular/router';
import { GameService } from '../../shared/game.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  list: Game [] = [];


  constructor(private route: ActivatedRoute, private gameService: GameService) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const gameId = params['platform'];
      this.list = this.gameService.getGamesByPlatform(gameId);
      console.log(gameId);
    });


  }
}
